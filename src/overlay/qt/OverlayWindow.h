#ifndef __OVERLAYWINDOW_H__
#define __OVERLAYWINDOW_H__

#include <qlabel.h>
#include <qmainwindow.h>
#include <qnamespace.h>
#include <qobject.h>
#include <qobjectdefs.h>
#include <qpoint.h>
#include <qwidget.h>
#include <qwindowdefs.h>
#include <QLabel>
#include <memory>

#include "utils/OverlayData.h"

// TODO: I really don't like this approach
// Way too much boilerplate in every implementation
class OverlayContent {
 public:
	virtual ~OverlayContent() = default;
	virtual void update_content() = 0;
	virtual void move_content(const QPoint& p) = 0;
	virtual void set_attribute(Qt::WidgetAttribute attr, bool on = true) = 0;
	virtual void set_window_flag(Qt::WindowType flag, bool on = true) = 0;
	virtual void set_style_sheet(const QString& style) = 0;
	virtual void on_mouse_move(QMouseEvent* event);
	virtual void on_mouse_press(QMouseEvent* event);

	virtual void show_content() = 0;
	virtual void hide_content() = 0;

 protected:
	std::shared_ptr<OverlayData> m_overlay_data;
	QPoint m_move_start_pos{};
};

class OverlayContentLabel : public QLabel, public OverlayContent {
	Q_OBJECT
 public:
	OverlayContentLabel(std::shared_ptr<OverlayData> data, QWidget* parent) : QLabel(parent) {
		this->m_overlay_data = data;
	}

	virtual void update_content() override;
	virtual void move_content(const QPoint& p) override;
	virtual void set_attribute(Qt::WidgetAttribute attr, bool on = true) override { this->setAttribute(attr, on); }
	virtual void set_window_flag(Qt::WindowType flag, bool on = true) override { this->setWindowFlag(flag, on); }
	virtual void set_style_sheet(const QString& style) override { this->setStyleSheet(style); }
	virtual void show_content() override { this->show(); }
	virtual void hide_content() override { this->hide(); }

 protected:
	void mouseMoveEvent(QMouseEvent* event) override;
	void mousePressEvent(QMouseEvent* event) override;
};

class OverlayWindow {
 public:
	OverlayWindow(std::shared_ptr<OverlayData> overlaydata, QWidget* parent = nullptr);
	virtual ~OverlayWindow();

	void set_pos(double x, double y);
	void set_size(double width, double length);

	void set_editing(bool state);
	void update();

 private:
	void create_overlay();

	QWidget* m_parent = nullptr;
	std::shared_ptr<OverlayData> m_overlay_data;
	std::shared_ptr<OverlayContent> m_overlay = nullptr;
	bool m_editing = false;
};

#endif	// __OVERLAYWINDOW_H__
