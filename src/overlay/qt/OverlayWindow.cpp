#include "OverlayWindow.h"
#include <qlabel.h>
#include <qnamespace.h>
#include <qwidget.h>
#include <QMouseEvent>

#include <iostream>
#include <memory>

OverlayWindow::OverlayWindow(std::shared_ptr<OverlayData> data, QWidget* parent) {
	this->m_overlay_data = data;
	this->m_parent = parent;
	this->create_overlay();
	this->m_overlay->show_content();
}

OverlayWindow::~OverlayWindow() {}

void OverlayWindow::create_overlay() {
	this->m_overlay.reset(new OverlayContentLabel(this->m_overlay_data, this->m_parent));
	this->m_overlay->set_window_flag(Qt::X11BypassWindowManagerHint);
	this->m_overlay->set_window_flag(Qt::FramelessWindowHint);
	this->m_overlay->set_window_flag(Qt::WindowStaysOnTopHint);
	this->m_overlay->set_window_flag(Qt::WindowDoesNotAcceptFocus);
	this->m_overlay->set_window_flag(Qt::WindowTransparentForInput);

	this->m_overlay->set_attribute(Qt::WA_TranslucentBackground, true);
	this->m_overlay->set_style_sheet("color: white;");
}

void OverlayWindow::update() { this->m_overlay->update_content(); }

void OverlayWindow::set_editing(bool state) {
	this->create_overlay();
	this->m_editing = state;
	if (state) {
		this->m_overlay->set_attribute(Qt::WA_TranslucentBackground, false);
		this->m_overlay->set_window_flag(Qt::WindowTransparentForInput, false);
	} else {
		this->m_overlay->set_attribute(Qt::WA_TranslucentBackground, true);
		this->m_overlay->set_window_flag(Qt::WindowTransparentForInput, true);
	}
	this->m_overlay->show_content();
}

void OverlayContent::on_mouse_press(QMouseEvent* event) {
	if (event->buttons() & Qt::LeftButton) {
		this->m_move_start_pos = event->pos();
	}
}

void OverlayContent::on_mouse_move(QMouseEvent* event) {
	if (event->buttons() & Qt::LeftButton) {
		this->move_content(event->pos() - this->m_move_start_pos);
	}
}

void OverlayContentLabel::mousePressEvent(QMouseEvent* event) { this->on_mouse_press(event); }
void OverlayContentLabel::mouseMoveEvent(QMouseEvent* event) { this->on_mouse_move(event); }
void OverlayContentLabel::move_content(const QPoint& pos) {
	auto new_pos = this->mapToParent(pos);
	this->move(new_pos);
	this->m_overlay_data->set_pos_x(new_pos.x());
	this->m_overlay_data->set_pos_y(new_pos.y());
}
void OverlayContentLabel::update_content() {
	this->move(this->m_overlay_data->get_pos_x(), this->m_overlay_data->get_pos_y());
	this->setFixedSize(this->m_overlay_data->get_width(), this->m_overlay_data->get_height());
	this->setText(QString::fromStdString(this->m_overlay_data->get_text()));
}
