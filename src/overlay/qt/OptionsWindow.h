#ifndef __OPTIONSWINDOW_H__
#define __OPTIONSWINDOW_H__

#include <qabstractitemmodel.h>
#include <qnamespace.h>
#include <qobject.h>
#include <qobjectdefs.h>
#include <qsortfilterproxymodel.h>
#include <qvariant.h>
#include <qwindowdefs.h>
#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QMainWindow>
#include <QSortFilterProxyModel>
#include <QTimer>
#include <QTreeWidgetItem>

#include <iterator>
#include <memory>
#include <vector>

#include "../../utils/GW2/GW2Api.h"
#include "../../utils/GW2/GW2Manager.h"
#include "../../utils/Logger.h"
#include "../../utils/POI.h"

#include "../../utils/CategoryManager.h"

#define STYLE_VALID "color: rgb(0, 255, 0);"
#define STYLE_INVALID "color: rgb(255, 0, 0);"

QT_BEGIN_NAMESPACE
namespace Ui {
class OptionsWindow;
class NewBuildDialog;
}  // namespace Ui
QT_END_NAMESPACE

class POIProxyModel : public QSortFilterProxyModel {
	Q_OBJECT
 public:
	POIProxyModel(QObject* parent = nullptr) {
		this->setRecursiveFilteringEnabled(true);
		this->setFilterCaseSensitivity(Qt::CaseInsensitive);
		this->setSortCaseSensitivity(Qt::CaseInsensitive);
	}

	bool POI_contains_map(POI* parent, int map_id) const {
		auto children = parent->get_children();
		for (auto iter = children->begin(); iter != children->end(); ++iter) {
			if ((*iter)->get_map_id() == map_id) return true;
			if (POI_contains_map(iter->get(), map_id)) return true;
		}
		return false;
	}

	bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const override {
		bool accept = QSortFilterProxyModel::filterAcceptsRow(sourceRow, sourceParent);
		auto index = this->sourceModel()->index(sourceRow, 0, sourceParent);
		POI* poi = static_cast<POI*>(index.internalPointer());
		auto gw2_data = GW2Manager::getInstance().get_link()->get_gw2_data();
		auto map_id = gw2_data.get_context()->mapId;
		bool map_filter = true;
		// test all children
		if (this->m_limit_map) {
			map_filter = POI_contains_map(poi, map_id);
		}
		return accept && map_filter;
	}

	void setSourceModel(QAbstractItemModel* model) override {
		QSortFilterProxyModel::setSourceModel(model);
		bool ret = connect(model, SIGNAL(updateAllData()), this, SLOT(updateAllSourceData()));
	}

	void set_limit_map(bool state) {
		this->m_limit_map = state;
		this->invalidateFilter();
		// TODO: invalidateFilter on map change
	}

 public slots:
	void updateAllSourceData() { emit this->dataChanged(QModelIndex(), QModelIndex()); }

 private:
	bool m_limit_map = false;
};
class POITreeViewModel : public QAbstractItemModel {
	Q_OBJECT
 signals:
	void updateAllData();

 public:
	POITreeViewModel(QObject* parent = nullptr) {
		this->m_pois = CategoryManager::getInstance().get_pois();
		// this->setHeaderData(0, Qt::Horizontal, "Name");
		// emit headerDataChanged(Qt::Horizontal, 0, 1);
	}

	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override {
		if (role == Qt::DisplayRole) {
			if (section == 0) {
				return "Name";
			} else if (section == 1) {
				return "ID";
			}
			return "undefined";
		}
		return QVariant();
	}

	int rowCount(const QModelIndex& parent = QModelIndex()) const override {
		int row_count = 0;
		if (parent.column() > 0) return 0;

		const poi_container* children = nullptr;
		if (!parent.isValid())
			children = this->m_pois;
		else
			children = static_cast<POI*>(parent.internalPointer())->get_children();

		for (auto iter = children->begin(); iter != children->end(); ++iter) {
			if (!(iter->get()->get_is_poi())) {
				++row_count;
			}
		}
		return row_count;
	}
	int columnCount(const QModelIndex& parent = QModelIndex()) const override { return 2; }
	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override {
		if (!index.isValid()) {
			return QVariant();
		}
		POI* poi = static_cast<POI*>(index.internalPointer());
		const poi_container* poi_list = this->m_pois;
		auto parent_ptr = poi->get_parent().lock();
		if (parent_ptr) {
			poi_list = parent_ptr->get_children();
		}
		auto it = poi_list->begin();
		std::advance(it, index.row());
		if (role == Qt::DisplayRole) {
			std::string data;
			if (index.column() == 0) {
				data = (*it)->get_display_name();
			} else if (index.column() == 1) {
				data = (*it)->get_name();
			}
			return QString::fromUtf8(data.c_str());
		}
		if (role == Qt::CheckStateRole && index.column() == 0) {
			return (*it)->is_enabled() ? Qt::Checked : Qt::Unchecked;
		}
		return QVariant();
	}
	bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override {
		if (index.isValid() && role == Qt::CheckStateRole) {
			POI* poi = static_cast<POI*>(index.internalPointer());
			poi->set_enabled(value == Qt::Checked);
			// update everything
			emit updateAllData();
			CategoryManager::getInstance().set_state_changed(true);
			return true;
		}
		return false;
	}
	Qt::ItemFlags flags(const QModelIndex& index) const override {
		if (!index.isValid()) return Qt::NoItemFlags;
		Qt::ItemFlags flags = Qt::ItemIsEnabled | Qt::ItemIsSelectable;

		if (index.column() == 0) flags |= Qt::ItemIsUserCheckable;

		return flags;
	}
	QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const override {
		if (!hasIndex(row, column, parent)) {
			return QModelIndex();
		}
		POI* child = nullptr;
		if (!parent.isValid()) {
			auto it = this->m_pois->begin();
			std::advance(it, row);
			child = it->get();
		} else {
			auto it = static_cast<POI*>(parent.internalPointer())->get_children()->begin();
			std::advance(it, row);
			child = it->get();
		}
		if (child) {
			return this->createIndex(row, column, child);
		}
		return QModelIndex();
	}
	QModelIndex parent(const QModelIndex& index) const override {
		if (!index.isValid()) {
			return QModelIndex();
		}

		POI* child = static_cast<POI*>(index.internalPointer());
		auto parent = child->get_parent().lock();
		if (child == parent.get() || !parent) {
			return QModelIndex();
		}
		const poi_container* parent_children = parent->get_children();
		auto parent_iter = parent_children->begin();
		for (; parent_iter != parent_children->end(); ++parent_iter) {
			if (parent_iter->get() == child) break;
		}
		if (parent_iter == parent_children->end()) return QModelIndex();
		int pos = std::distance(parent_children->begin(), parent_iter);
		return createIndex(pos, 0, parent.get());
	}

 private:
	const poi_container* m_pois = nullptr;
};

class OptionsWindow : public QMainWindow {
	Q_OBJECT

 public:
	OptionsWindow(QWidget* parent = nullptr);
	~OptionsWindow() = default;

 public slots:
	void set_all(bool state);

 private:
	void save_settings();
	void load_settings();
	void add_build_dialog();
	void add_build(const std::string& name, const std::string& desc, const std::string& val);
	void add_build(const QString& name, const QString& desc, const QString& val);
	void copy_build();
	void save_builds();
	void load_builds();
	void select_helper_path();
	std::shared_ptr<Ui::OptionsWindow> m_ui;
	std::map<std::string, QLineEdit*> m_options_map;
	std::map<GW2Permission, QLabel*> m_permission_map;
	void showEvent(QShowEvent* ev) override;
	void copy_from_api();
	void update_performance();
	void on_tab_change(int index);
	void on_build_select();
	void filter_pois(const QString& text);

	bool m_disable_build_save = false;
	QTimer* m_update_timer = nullptr;
	POIProxyModel* m_proxy_model = nullptr;
};

class NewBuildDialog : public QDialog {
 public:
	NewBuildDialog(QDialog* p = nullptr);

	std::shared_ptr<Ui::NewBuildDialog> m_ui;
};

#endif	// __OPTIONSWINDOW_H__
