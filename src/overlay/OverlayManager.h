#ifndef __OVERLAYMANAGER_H_
#define __OVERLAYMANAGER_H_

#include <qnamespace.h>
#include <iostream>
#include <memory>
#include <queue>
#include <string>
#include <tuple>
#include <unordered_map>
#include <utility>
#include <vector>

#include "overlay/qt/OverlayWindow.h"
#include "utils/Lock.h"
#include "utils/Logger.h"
#include "utils/OverlayData.h"

#include <QLabel>

class OverlayManager {
 public:
	static OverlayManager& getInstance() {
		static OverlayManager instance;
		return instance;
	}
	void add_overlay(std::shared_ptr<OverlayData> data, const std::string& name) {
		LOG_DEBUG << "Adding new overlay \"" << name << "\" to queue";
		write_lock lock(this->m_overlay_queue_lock);
		this->m_overlay_queue.push({data, name});
	}
	void remove_overlay(const std::string& name) {
		write_lock lock(this->m_overlay_queue_lock);
		this->m_remove_overlay_queue.push(name);
	}
	void update_overlays() {
		write_lock lock(this->m_overlay_queue_lock);
		write_lock lock_overlay(this->m_overlay_lock);
		while (this->m_overlay_queue.size()) {
			auto data = this->m_overlay_queue.front();
			LOG_DEBUG << "Adding new overlay " << data.second;
			this->m_overlay_queue.pop();
			this->m_overlay_data[data.second] = std::shared_ptr<OverlayWindow>(new OverlayWindow(data.first));
			if (this->m_edit_mode) {
				this->m_overlay_data[data.second]->set_editing(true);
			}
		}
		while (this->m_remove_overlay_queue.size()) {
			auto data = this->m_remove_overlay_queue.back();
			this->m_remove_overlay_queue.pop();
			this->m_overlay_data.erase(data);
		}
		lock.unlock();
		for (auto iter = this->m_overlay_data.begin(); iter != this->m_overlay_data.end(); ++iter) {
			iter->second->update();
		}
	}

	void toggle_editing() { this->set_editing(!this->m_edit_mode); }

	void set_editing(bool state) {
		read_lock lock(this->m_overlay_lock);
		for (auto iter = this->m_overlay_data.begin(); iter != this->m_overlay_data.end(); ++iter) {
			iter->second->set_editing(state);
		}
		this->m_edit_mode = state;
	}

 private:
	OverlayManager() = default;
	std::unordered_map<std::string, std::shared_ptr<OverlayWindow>> m_overlay_data;
	std::queue<std::pair<std::shared_ptr<OverlayData>, std::string>> m_overlay_queue;
	std::queue<std::string> m_remove_overlay_queue;

	bool m_edit_mode = false;

	mutable mutex_type m_overlay_queue_lock;
	mutable mutex_type m_overlay_lock;
};

#endif	// __OVERLAYMANAGER_H_
