#ifndef __OVERLAYCONFIG_H__
#define __OVERLAYCONFIG_H__

#define USE_LINK_THREAD

#define FPS_TO_MS(x) (1.0 / x * 1000)
#define FPS_TO_US(x) (MS_TO_US(FPS_TO_MS(x)))

#define MS_TO_US(x) (x * 1000)
#define LOOP_MIN_TIME_US FPS_TO_US(60.0)

#endif
