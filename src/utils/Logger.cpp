#include "Logger.h"
#include <fstream>

loglevel Logger::verbosity = LOGLEVEL_WARN;
std::unordered_set<std::string> Logger::visible_tags = {"DEFAULT"};
std::ostream* Logger::output_stream = &std::cout;
std::ofstream Logger::output_file_stream("overlay.log", std::ofstream::out);

void Logger::visible_tag_add(const std::string& tag) { Logger::visible_tags.emplace(tag); }

void Logger::visible_tag_remove(const std::string& tag) {
	try {
		Logger::visible_tags.erase(tag);
	} catch (...) {
	}
}

Logger::~Logger() {
	if (this->m_loglevel >= Logger::verbosity) {
		if (this->m_color_enabled) {
			this->operator<<(color::Color(color::TERM_FG_COLOR_DEFAULT));
		}
		this->operator<<("\n");
		if (Logger::output_file_stream.is_open()) {
			Logger::output_file_stream << std::endl;
		}
		*output_stream << this->buf.str();
	}
}

std::string Logger::get_label() const {
	std::string label;
	switch (this->m_loglevel) {
		case LOGLEVEL_DEBUG:
			label = "DEBUG";
			break;
		case LOGLEVEL_INFO:
			label = "INFO";
			break;
		case LOGLEVEL_WARN:
			label = "WARNING";
			break;
		case LOGLEVEL_ERROR:
			label = "ERROR";
			break;
		case LOGLEVEL_CRITICAL:
			label = "CRITICAL";
			break;
		default:
			break;
	}
	return label;
}

color::Color Logger::get_color() const {
	color::Color color;
	switch (this->m_loglevel) {
		case LOGLEVEL_TRACE:
			color.set_color(color::TERM_FG_COLOR_PURPLE);
			break;
		case LOGLEVEL_DEBUG:
			color.set_color(color::TERM_FG_COLOR_CYAN);
			break;
		case LOGLEVEL_INFO:
			color.set_color(color::TERM_FG_COLOR_BLUE);
			break;
		case LOGLEVEL_WARN:
			color.set_color(color::TERM_FG_COLOR_YELLOW);
			break;
		case LOGLEVEL_ERROR:
			color.set_color(color::TERM_FG_COLOR_RED);
			break;
		case LOGLEVEL_CRITICAL:
			color.set_color(color::TERM_FG_COLOR_BRIGHT_RED);
			break;
		default:
			break;
	}
	return color;
}
