#ifndef __LOGGER_H_
#define __LOGGER_H_

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <unordered_set>
#include <vector>

namespace color {
enum Color_code {
	TERM_FG_COLOR_RED = 31,
	TERM_FG_COLOR_BRIGHT_RED = 91,
	TERM_FG_COLOR_GREEN = 32,
	TERM_FG_COLOR_YELLOW = 33,
	TERM_FG_COLOR_BLUE = 34,
	TERM_FG_COLOR_CYAN = 36,
	TERM_FG_COLOR_DEFAULT = 39,
	TERM_FG_COLOR_PURPLE = 35
};

class Color {
 public:
	explicit Color(Color_code color = TERM_FG_COLOR_DEFAULT) : m_color(color) {}
	void set_color(Color_code color) { this->m_color = color; }
	friend std::ostream& operator<<(std::ostream& os, const Color& color) {
		return os << "\033[" << color.m_color << "m";
	}

 private:
	Color_code m_color;
};

}  // namespace color

enum loglevel {
	LOGLEVEL_TRACE,
	LOGLEVEL_DEBUG,
	LOGLEVEL_INFO,
	LOGLEVEL_WARN,
	LOGLEVEL_ERROR,
	LOGLEVEL_CRITICAL,
	LOGLEVEL_COUNT
};

class Logger {
 public:
	explicit Logger(loglevel level = LOGLEVEL_DEBUG, bool enable_color = true,
					const std::unordered_set<std::string>& tags = {"DEFAULT"}) {
		this->m_loglevel = level;
		this->m_color_enabled = enable_color;
		this->m_tags = tags;
		if (enable_color) {
			operator<<(this->get_color());
		}
		auto now = std::chrono::system_clock::now();
		auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()) % 1000;

		auto t_c = std::chrono::system_clock::to_time_t(now);
		operator<<("[") << (std::put_time(std::localtime(&t_c), "%T"));
		operator<<(".") << std::setfill('0') << std::setw(3) << ms.count() << "]";

		operator<<("[" + this->get_label() + "]");
	}

	virtual ~Logger();

	template <class T>
	Logger& operator<<(const T& msg) {
		if (this->m_loglevel >= Logger::verbosity) {
			std::vector<std::string> intersection;
			std::set_intersection(Logger::visible_tags.begin(), Logger::visible_tags.end(), this->m_tags.begin(),
								  this->m_tags.end(), std::back_inserter(intersection));
			if (intersection.size() > 0) {
				buf << msg;
				if (Logger::output_file_stream.is_open()) {
					Logger::output_file_stream << msg;
				} else {
					std::cout << "File invalid!" << std::endl;
				}
			}
		}
		return *this;
	}

	static void set_verbosity(loglevel verbosity) { Logger::verbosity = verbosity; }
	static void set_output_stream(std::ostream* stream) { Logger::output_stream = stream; }

	static void set_output_file(const std::string& path) {
		if (Logger::output_file_stream.is_open()) {
			Logger::output_file_stream.close();
		}
		Logger::output_file_stream.open(path, std::ofstream::out);
	}

	static void visible_tag_add(const std::string& tag);
	static void visible_tag_remove(const std::string& tag);

 private:
	loglevel m_loglevel;
	static loglevel verbosity;
	static std::unordered_set<std::string> visible_tags;
	static std::ostream* output_stream;
	static std::ofstream output_file_stream;
	bool m_color_enabled;
	std::unordered_set<std::string> m_tags;
	std::stringstream buf;

	std::string get_label() const;
	color::Color get_color() const;
};

#define LOGGER_PREFIX "[" __FILENAME__ ":" << __LINE__ << "] "

#define LOG_TRACE Logger(LOGLEVEL_TRACE) << LOGGER_PREFIX
#define LOG_DEBUG Logger(LOGLEVEL_DEBUG) << LOGGER_PREFIX
#define LOG_INFO Logger(LOGLEVEL_INFO) << LOGGER_PREFIX
#define LOG_WARN Logger(LOGLEVEL_WARN) << LOGGER_PREFIX
#define LOG_ERROR Logger(LOGLEVEL_ERROR) << LOGGER_PREFIX
#define LOG_CRITICAL Logger(LOGLEVEL_CRITICAL) << LOGGER_PREFIX

#endif

