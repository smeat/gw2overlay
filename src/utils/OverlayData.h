#ifndef __OVERLAY_DATA_H__
#define __OVERLAY_DATA_H__

#include <string>
#include "utils/Lock.h"

class OverlayData {
 public:
	double get_pos_x() const {
		read_lock lock(this->m_mutex);
		return this->m_pos_x;
	}
	double get_pos_y() const {
		read_lock lock(this->m_mutex);
		return this->m_pos_y;
	}
	double get_width() const {
		read_lock lock(this->m_mutex);
		return this->m_width;
	}
	double get_height() const {
		read_lock lock(this->m_mutex);
		return this->m_height;
	}
	void set_pos_x(double val) {
		write_lock lock(this->m_mutex);
		this->m_pos_x = val;
	}
	void set_pos_y(double val) {
		write_lock lock(this->m_mutex);
		this->m_pos_y = val;
	}
	void set_width(double val) {
		write_lock lock(this->m_mutex);
		this->m_width = val;
	}
	void set_height(double val) {
		write_lock lock(this->m_mutex);
		this->m_height = val;
	}
	std::string get_text() const {
		read_lock lock(this->m_mutex);
		return this->m_text;
	}
	void set_text(const std::string& text) {
		write_lock lock(this->m_mutex);
		this->m_text = text;
	}

 protected:
	double m_pos_x = 0;
	double m_pos_y = 0;
	double m_width = 100;
	double m_height = 100;
	std::string m_text;

	mutable mutex_type m_mutex;
};

#endif
