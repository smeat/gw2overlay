#ifndef __ENCOUNTER_H_
#define __ENCOUNTER_H_

class Encounter {
 public:
	virtual void on_combat_begin() = 0;
	virtual void on_combat_end() = 0;
	virtual void get_objects() = 0;
	virtual void update() = 0;
};

#endif
