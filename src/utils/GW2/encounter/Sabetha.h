#ifndef __SABETHA_H_
#define __SABETHA_H_

#include "Encounter.h"

#include "overlay/OverlayManager.h"

#include <stdint.h>
#include <chrono>
#include <cmath>
#include <sstream>
#include <string>
#include <unordered_map>

#include "utils/GW2/GW2Manager.h"
#include "utils/Logger.h"

#define SABETHA_MAP_ID 1062
#define CANNON_TIMER (30 * 1000)

#define SABETHA_TRIGGER_X -130
#define SABETHA_TRIGGER_Y 85

#define SABETHA_TRIGGER_R 50

enum directions_e { NORTH, SOUTH, WEST, EAST };

const directions_e CANON_DIRECTION[] = {directions_e::SOUTH, directions_e::WEST,  directions_e::NORTH,
										directions_e::EAST,	 directions_e::SOUTH, directions_e::NORTH,
										directions_e::WEST,	 directions_e::EAST};

const std::unordered_map<directions_e, std::string> DIRECTION_NAME_MAP = {{directions_e::NORTH, "North"},
																		  {directions_e::SOUTH, "South"},
																		  {directions_e::WEST, "West"},
																		  {directions_e::EAST, "East"}};

class SabethaEncounter : public Encounter {
 public:
	SabethaEncounter() {
		LOG_DEBUG << "Loading Sabetha encounter!";
		this->m_overlay_data.reset(new OverlayData);
		this->m_overlay_data->set_text("");
		this->m_overlay_data->set_pos_x(2000);
		this->m_overlay_data->set_width(400);
		OverlayManager::getInstance().add_overlay(this->m_overlay_data, "SabethaEncounter");
	}
	virtual ~SabethaEncounter() { OverlayManager::getInstance().remove_overlay("SabethaEncounter"); }
	virtual void on_combat_begin() override {
		LOG_DEBUG << "[SABETHA] Beginning combat!";
		// check if in range
		GW2Link* gw_link = GW2Manager::getInstance().get_link();
		double player_x = gw_link->get_gw2_data().fAvatarPosition[0];
		double player_y = gw_link->get_gw2_data().fAvatarPosition[2];
		double d = std::sqrt(std::pow(player_x - SABETHA_TRIGGER_X, 2) + std::pow(player_y - SABETHA_TRIGGER_Y, 2));
		if (d < SABETHA_TRIGGER_R) {
			LOG_DEBUG << "[SABETHA] In range! Starting Timer!";
			this->m_encounter_start = this->get_system_ms();
		}
	}
	virtual void on_combat_end() override {
		std::cout << "[SABETHA] Combat end!";
		this->m_encounter_start = 0;
		this->m_overlay_data->set_text("");
	}
	virtual void get_objects() override {}
	virtual void update() override {
		if (this->get_system_ms() - this->m_last_update < 1000 || !this->m_overlay_data ||
			this->m_encounter_start == 0) {
			return;
		}
		int time_till_next_cannon = CANNON_TIMER - (get_encounter_time() % CANNON_TIMER);
		int current_cannon = get_encounter_time() / CANNON_TIMER % 4;
		std::stringstream overlay_text;
		directions_e cannon_direction = CANON_DIRECTION[current_cannon];
		const std::string cannon_direction_str = DIRECTION_NAME_MAP.find(cannon_direction)->second;
		overlay_text << "Next cannon at" << cannon_direction_str << " in " << time_till_next_cannon
					 << "\nCurrent Time: " << this->get_encounter_time();
		this->m_overlay_data->set_text(overlay_text.str());
		this->m_last_update = this->get_system_ms();
	}

	uint64_t get_encounter_time() { return this->get_system_ms() - this->m_encounter_start; }
	uint64_t get_system_ms() {
		auto now = std::chrono::system_clock::now().time_since_epoch();
		return std::chrono::duration_cast<std::chrono::milliseconds>(now).count();
	}

 private:
	uint64_t m_encounter_start = 0;
	uint64_t m_last_update = 0;
	std::shared_ptr<OverlayData> m_overlay_data;
};

#endif
