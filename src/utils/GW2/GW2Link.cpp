#include "GW2Link.h"

#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>

#include <fcntl.h>
#include <sys/mman.h>
#include <wchar.h>
#include <codecvt>
#include <locale>

#include "../PerformanceStats.h"

GW2Link::GW2Link() {
	char memname[256];
	snprintf(memname, 256, "/MumbleLink.%d", getuid());
	// TODO: create if not exist https://github.com/mumble-voip/mumble/blob/master/plugins/link/link-posix.cpp#L177
	int shmfd = shm_open(memname, O_RDWR, S_IRUSR | S_IWUSR);
	bool created = false;
	if (shmfd < 0) {
		created = true;
		shmfd = shm_open(memname, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
	}
	if (shmfd < 0) {
		fprintf(stderr, "Failed to shm_open\n");
		fprintf(stderr, "%s\n", strerror(errno));
	}

	if (created) {
		if (ftruncate(shmfd, sizeof(LinkedMem)) != 0) {
			fprintf(stderr, "Failed to resize shared memory\n");
			close(shmfd);
			shmfd = -1;
		}
	}

	this->m_gw2_data = (LinkedMem *)(mmap(NULL, sizeof(LinkedMem), PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0));
	if (this->m_gw2_data == (void *)(-1)) {
		printf("Failed mmap!\n");
		this->m_gw2_data = new LinkedMem;
	}
	this->m_socket = this->create_socket();
	memset(this->m_gw2_data, 0, sizeof(LinkedMem));
	printf("Init GW2Link with Linkedmem size %lu, context size %lu total %lu\n", sizeof(LinkedMem),
		   sizeof(MumbleContext), sizeof(LinkedMem) + sizeof(MumbleContext));
}

std::string utf16ToUtf8(const std::u16string &str) {
	try {
		std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> conv;
		return conv.to_bytes(str);
	} catch (std::range_error &) {
		return {};
	}
}

std::wstring utf16ToUtf32(const std::u16string &str) {
	try {
		std::string u8string = utf16ToUtf8(str);
		std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> conv;
		return conv.from_bytes(u8string);
	} catch (std::range_error &) {
		return {};
	}
}

void GW2Link::update_gw2(bool block) {
	auto loop_begin = std::chrono::high_resolution_clock::now();
	struct sockaddr_in si_other;
	int len = 0, slen = sizeof(si_other);
	LinkedMemNet data;
	int flags = 0;
	if (block) {
		int timeout = 200;
		struct timeval tv;
		tv.tv_sec = timeout / 1000;
		tv.tv_usec = (timeout % 1000) * 1000;
		fd_set rfds;
		FD_ZERO(&rfds);
		FD_SET(this->m_socket, &rfds);
		::select(FD_SETSIZE, &rfds, nullptr, nullptr, &tv);
	} else {
		flags |= MSG_DONTWAIT;
	}
	if ((len = ::recvfrom(this->m_socket, &data, sizeof(data), MSG_DONTWAIT, (struct sockaddr *)&si_other,
						  (socklen_t *)&slen)) > 0) {
		// clang-format off
		/*
		printf("uiVersion %d, uiTick %d\n", data.uiVersion, data.uiTick);
		printf("fAvatarPosition (%f %f %f) fAvatarFront (%f %f %f) fAvatarTop (% f % f % f)\n", data.fAvatarPosition[0], data.fAvatarPosition[1],
			   data.fAvatarPosition[2],
			   data.fAvatarFront[0], data.fAvatarFront[1], data.fAvatarFront[2],
			   data.fAvatarTop[0], data.fAvatarTop[1], data.fAvatarTop[2]);
		printf("fCameraPosition (%f %f %f) fCameraFront (%f %f %f) fCameraTop (% f % f % f)\n", data.fCameraPosition[0], data.fCameraPosition[1],
			   data.fCameraPosition[2],
			   data.fCameraFront[0], data.fCameraFront[1], data.fCameraFront[2],
			   data.fCameraTop[0], data.fCameraTop[1], data.fCameraTop[2]);
		printf("Contextlen %d\n", data.context_len);
		printf("Packet len %d should be %lu, diff % d ctx %lu\n ", len, sizeof(LinkedMem), int(sizeof(LinkedMem) - len), sizeof(MumbleContext));
		std::wcout << L"Name: " << (wchar_t*)data.name << std::endl;
		std::cout << "Name2: " << utf16ToUtf8((char16_t*)data.name) << std::endl;
		/**/
		// clang-format on

		this->m_gw2_data->operator=(data);
		this->m_last_update = std::chrono::high_resolution_clock::now();
		if (data.uiTick == 0) {
			std::cout << "UiTick is 0. If this message doesn't stop, make sure the mumble script is running!"
					  << std::endl;
		}
		auto time = std::chrono::duration_cast<std::chrono::microseconds>(this->m_last_update - loop_begin);
		PerformanceStats::getInstance().set_time("link", time.count());
	}
}

const LinkedMem GW2Link::get_gw2_data() const { return *this->m_gw2_data; }

int GW2Link::create_socket() {
	int fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd == -1) {
		printf("Failed to create socket\n");
		return fd;
	}
	struct sockaddr_in servaddr;
	memset(&servaddr, 0, sizeof(servaddr));
	// Filling server information
	servaddr.sin_family = AF_INET;	// IPv4
	servaddr.sin_addr.s_addr = INADDR_ANY;
	servaddr.sin_port = htons(7070);

	// TODO: proper getaddr
	if (::bind(fd, (const struct sockaddr *)&servaddr, sizeof(servaddr)) == -1) {
		printf("Failed to bind socket\n");
		::close(fd);
		fd = -1;
	}

	printf("Created socket\n");
	return fd;
}
void GW2Link::start_update_thread() {
	this->m_thread_running = true;

	auto thread_func = [this]() {
		while (this->m_thread_running) {
			this->update_gw2(true);
		}
	};

	this->m_update_thread = std::thread(thread_func);
}

std::string LinkedMem::get_identity() const {
	char c = this->identity[0];
	int i = 0;
	std::stringstream ss;
	while (c != 0 && i < 256) {
		i += 1;
		ss << c;
		c = this->identity[i];
	}
	return ss.str();
}

const MumbleContext *LinkedMem::get_context() const { return (MumbleContext *)this->context; }
LinkedMem LinkedMem::operator=(LinkedMemNet other) {
	this->uiVersion = other.uiVersion;
	this->uiTick = other.uiTick;
	memcpy(this->fAvatarPosition, other.fAvatarPosition, sizeof(float) * 3);
	memcpy(this->fAvatarFront, other.fAvatarFront, sizeof(float) * 3);
	memcpy(this->fAvatarTop, other.fAvatarTop, sizeof(float) * 3);
	std::wstring wname = utf16ToUtf32(other.name);
	memcpy(this->name, wname.data(), 256 * sizeof(wchar_t));
	memcpy(this->fCameraPosition, other.fCameraPosition, sizeof(float) * 3);
	memcpy(this->fCameraFront, other.fCameraFront, sizeof(float) * 3);
	memcpy(this->fCameraTop, other.fCameraTop, sizeof(float) * 3);
	std::wstring wident = utf16ToUtf32(other.identity);
	memcpy(this->identity, wident.data(), 256 * sizeof(wchar_t));
	this->context_len = other.context_len;
	memcpy(this->context, other.context, 256);
	return *this;
}
